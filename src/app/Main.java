package app;
/*
 * Sam Olson
 * 11/2/17
 * MySql server management app
 */
import ui.ProductManagerFrame;

public class Main {

    @SuppressWarnings("unused")
	public static void main(String[] args) {
        ProductManagerFrame frame = new ProductManagerFrame();
    }
}