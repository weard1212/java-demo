package ui;
/*
 * Sam Olson
 * 11/2/17
 * MySql server management app
 */
import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import business.Product;
import db.ProductDB;
import db.DBException;

@SuppressWarnings("serial")
public class ProductManagerFrame extends JFrame {
    private JTable productTable;
    private ProductTableModel productTableModel;
    
    public ProductManagerFrame() {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException |
                 IllegalAccessException | UnsupportedLookAndFeelException e) {
            System.out.println(e);
        }        
        setTitle("Product Manager");
        setSize(768, 384);
        setLocationByPlatform(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        add(buildButtonPanel(), BorderLayout.SOUTH);
        productTable = buildProductTable();
        add(new JScrollPane(productTable), BorderLayout.CENTER);
        setVisible(true);        
    }
    
    private JPanel buildButtonPanel() {
        JPanel panel = new JPanel();
        
        JButton addButton = new JButton("Add");
        addButton.setToolTipText("Add product");
        addButton.addActionListener((ActionEvent) -> {
            try {
            	Product product = new Product();
				doAddButton(product);
			} catch (Exception e) {
				e.printStackTrace();
			}
        });
        panel.add(addButton);
        
        JButton editButton = new JButton("Edit");
        editButton.setToolTipText("Edit selected product");
        editButton.addActionListener((ActionEvent) -> {
            try {
				doEditButton();
			} catch (Exception e) {
				e.printStackTrace();
			}
        });
        panel.add(editButton);
        
        JButton deleteButton = new JButton("Delete");
        deleteButton.setToolTipText("Delete selected product");
        deleteButton.addActionListener((ActionEvent) -> {
            doDeleteButton();
        });
        panel.add(deleteButton);
        panel.add(editButton);
        
        JButton printButton = new JButton("Print");
        printButton.setToolTipText("Print selected product");
        printButton.addActionListener((ActionEvent) -> {
            doPrintButton();
        });
        panel.add(printButton);        
        return panel;
    }
    
    private void doAddButton(Product product){
    	JPanel addPanel = new JPanel();
    	addPanel.setLayout(new BoxLayout(addPanel, BoxLayout.Y_AXIS));
    	
    	JLabel codelabel = new JLabel("Code:");
    	addPanel.add(codelabel);
    	JTextField code = new  JTextField();
    	code.setToolTipText("Enter the one word code for the product.");
    	addPanel.add(code);
    	
    	JLabel descriptionlabel = new JLabel("Description:");
    	addPanel.add(descriptionlabel);
    	JTextField description = new  JTextField();
    	description.setToolTipText("Enter the description for the product.");
    	addPanel.add(description);
    	
    	JLabel pricelabel = new JLabel("Price:");
    	addPanel.add(pricelabel);
    	JTextField price = new  JTextField();
    	price.setToolTipText("Enter price for the product.");
    	addPanel.add(price);
    	if (product != null){
    		code.setText(product.getCode());
    		description.setText(product.getDescription());
    		price.setText(String.valueOf(product.getPrice()));
    	}
    	int fail = 1;
		while (fail  == 1){
    	int confirm = JOptionPane.showConfirmDialog(this, addPanel, "Add to Server", JOptionPane.OK_CANCEL_OPTION);
    	if (confirm == JOptionPane.CANCEL_OPTION){fail = 0;}
    	if (confirm == JOptionPane.OK_OPTION){
    		try{
    			String codeStr = code.getText();
    			product.setCode(codeStr);
    			String descriptionStr = description.getText();
    			product.setDescription(descriptionStr);
    			Double priceDoub = Double.parseDouble(price.getText());
    			product.setPrice(priceDoub);
    			if (product.getId() > 0){
    				ProductDB.update(product);
    			}
    			else{
    				ProductDB.add(product);
    			}
        		fail = 0;
        		productTableModel.databaseUpdated();
    		}
    		catch(Exception e){
    			JOptionPane.showMessageDialog(this, "Invalid data entered");
    		}
    	}
    }
    }
    
    private void doEditButton() throws DBException {
    	int selectedRow = productTable.getSelectedRow();
        if (selectedRow == -1) {
            JOptionPane.showMessageDialog(this,
                    "No product is currently selected.", 
                    "No product selected", JOptionPane.ERROR_MESSAGE);
        } else {
            Product product = productTableModel.getProduct(selectedRow);
            doAddButton(product);
        }
    }
    
    private void doDeleteButton() {
        int selectedRow = productTable.getSelectedRow();
        if (selectedRow == -1) {
            JOptionPane.showMessageDialog(this,
                    "No product is currently selected.", 
                    "No product selected", JOptionPane.ERROR_MESSAGE);
        } else {
            Product product = productTableModel.getProduct(selectedRow);
            int option = JOptionPane.showConfirmDialog(this,
                    "Are you sure you want to delete " + 
                            product.getDescription() + " from the database?",
                    "Confirm delete", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_OPTION) {
                try {                    
                    ProductDB.delete(product);
                    productTableModel.databaseUpdated();
                } catch (DBException e) {
                    System.out.println(e);
                }
            }
        }
    }
    private void doPrintButton() {
        int selectedRow = productTable.getSelectedRow();
        if (selectedRow == -1) {
            JOptionPane.showMessageDialog(this,
                    "No product is currently selected.", 
                    "No product selected", JOptionPane.ERROR_MESSAGE);
        } else {
            Product product = productTableModel.getProduct(selectedRow);
            JOptionPane.showMessageDialog(this, product.getDescription() + " was sent to the printer");
            }
        }
    
    private JTable buildProductTable() {
        productTableModel = new ProductTableModel();
        JTable table = new JTable(productTableModel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setBorder(null);
        return table;
    }
}